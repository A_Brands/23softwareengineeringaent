import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class ThreadNumbers {

	public static void main(String[] args) {
		// Start threads in a thread pool
		ExecutorService executor = Executors.newFixedThreadPool(1000);

		for (int i = 1; i <= 4; i++) {
			executor.execute(new PrintNumbers(i));
		}

		executor.shutdown();
	}

	static class PrintNumbers implements Runnable {

		private static Lock lock = new ReentrantLock();
		private static Condition condition = lock.newCondition();
		private static int count;
		private int number;

		public PrintNumbers(int number) {
			// The number to print
			this.number = number;
			// Increment the thread counter for ordering
			count++;
		}

		@Override
		public void run() {
			// Obtain lock
			lock.lock();
			try {
				// Wait until it is this thread's turn
				while (this.number < count) {
					condition.await();
				}
				// Print the number twice
				System.out.print(this.number);
				System.out.print(this.number + " ");
				// Decrement the counter for following threads
				count--;
				// Signal other threads so they might continue
				condition.signalAll();
			} catch (Exception e) {
				e.printStackTrace();
			}
			// Release lock
			lock.unlock();
		}

	}

}
