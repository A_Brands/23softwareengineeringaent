import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ThreadCooperation {

	private static Account account = new Account();

	public static void main(String[] args) {
		// Create a thread pool with two threads
		ExecutorService executor = Executors.newFixedThreadPool(2);
		executor.execute(new DepositTask());
		executor.execute(new WithdrawTask());
		executor.shutdown();

		System.out.println("Thread 1\t\tThread 2\t\tBalance");
	}

	// A task for adding an amount to the account
	public static class DepositTask implements Runnable {

		@Override
		public void run() {
			try { // Purposely delay it to let the withdraw method proceed
				while (true) {
					account.deposit((int) (Math.random() * 10) + 1);
					Thread.sleep(1000);
				}
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

	}

	// A task for subtracting an amount from the account
	public static class WithdrawTask implements Runnable {

		@Override
		public void run() {
			while (true) {
				account.withdraw((int) (Math.random() * 10) + 1);
			}
		}

	}

	// An inner class for account
	private static class Account {

		private int balance = 0;

		public int getBalance() {
			return this.balance;
		}

		public void withdraw(int amount) {
			try {
				while (this.balance < amount) {
					// Wait for deposit
					synchronized (this) {
						this.wait();
					}
				}

				this.balance -= amount;
				System.out.println("\t\t\tWithdraw " + amount + "\t\t"
						+ this.getBalance());
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

		public void deposit(int amount) {
			try {
				this.balance += amount;
				System.out.println("Deposit " + amount + "\t\t\t\t\t"
						+ this.getBalance());

				// Signal waiting threads
				synchronized (this) {
					this.notifyAll();
				}
			} catch (Exception e) {
			}
		}

	}

}
