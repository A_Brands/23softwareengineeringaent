import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class OneThousandThreads {

	// The sum for threads to add to
	private static Integer sum = new Integer(0);
	// The task to run in threads
	private static Runnable adder = new Runnable() {
		@Override
		public void run() {
			synchronized (sum) {
				int value = sum.intValue() + 1;
				sum = new Integer(value);
			}
		}
	};

	public static void main(String[] args) {
		// Thread pool for the threads to run
		ExecutorService executor = Executors.newFixedThreadPool(1000);

		// Record start value and time
		int start = sum.intValue();
		long time = System.currentTimeMillis();

		// Execute the 1000 threads
		for (int i = 0; i < 1000; i++) {
			executor.execute(adder);
		}

		// Wait until all threads finish
		executor.shutdown();
		while (!executor.isTerminated()) {
		}

		// Display results
		time = System.currentTimeMillis() - time;
		System.out.println("Went from " + start + " to " + sum + " in " + time
				+ "ms");
	}

}
