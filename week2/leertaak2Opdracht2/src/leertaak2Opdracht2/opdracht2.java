package leertaak2Opdracht2;

import java.util.Enumeration;

import javax.swing.tree.DefaultMutableTreeNode;

public class opdracht2 {
	public static DefaultMutableTreeNode makeTree() {
		DefaultMutableTreeNode person = new DefaultMutableTreeNode("person");
		DefaultMutableTreeNode employee = new DefaultMutableTreeNode("employee");
		DefaultMutableTreeNode customer = new DefaultMutableTreeNode("customer");
		DefaultMutableTreeNode sales_rep = new DefaultMutableTreeNode(
				"sales_rep");
		DefaultMutableTreeNode engineer = new DefaultMutableTreeNode("engineer");
		DefaultMutableTreeNode us_customer = new DefaultMutableTreeNode(
				"us_customer");
		DefaultMutableTreeNode non_us_customer = new DefaultMutableTreeNode(
				"non_us_customer");
		DefaultMutableTreeNode local_customers = new DefaultMutableTreeNode(
				"local_customer");
		DefaultMutableTreeNode regional_customers = new DefaultMutableTreeNode(
				"regional_customers");

		// person as root
		person.add(employee);
		person.add(customer);

		// employee as root
		employee.add(sales_rep);
		employee.add(engineer);

		// customer as root
		customer.add(us_customer);
		customer.add(non_us_customer);

		// us_customer as root
		us_customer.add(local_customers);
		us_customer.add(regional_customers);

		return person; //person = root
	}

	public static void main(String[] args)
	{
		DefaultMutableTreeNode tree = makeTree();
		System.out.println("Breadth-first: " + order(tree, "breadth-first"));
		System.out.println("Preorder: " + order(tree, "preorder"));
		System.out.println("postorder: " + order(tree, "postorder"));
	}
	
	public static String order(DefaultMutableTreeNode tree , String order) {
		String s = "";

		Enumeration e = null;
		//determine order
		if(order == "preorder")
		{
			e = tree.preorderEnumeration();
		}
		else if(order == "postorder")
		{
			e = tree.postorderEnumeration();
		}
		else if(order == "breadth-first")
		{
			e = tree.breadthFirstEnumeration();
		}
		
		//iterate
		while(e.hasMoreElements()) {
			s += e.nextElement().toString() + (e.hasMoreElements() ? ", " : "");
			
		}
		return s;
	}
}
