public class SnelheidOefening {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		GetalRij gr = new GetalRij(100000, 200000);
		long start;
		long end;

		// ziterinA
		start = tijd();
		boolean zitErin = gr.zitErinA(65000);
		end = tijd();
		long tijd = end - start;
		System.out.println("A: " + zitErin + "     Tijd: " + tijd);

		System.gc();
		// ziterinB

		start = tijd();
		System.out.print("B: " + gr.zitErinB(65000) + "     Tijd: ");
		end = tijd();
		System.out.println((end - start));

		System.gc();
		gr.sorteer();
		// zitErinC
		
		start = tijd();
		System.out.print("C: " + gr.zitErinC(65000) + "     Tijd: ");
		end = tijd();
		System.out.println((end - start));

		System.gc();
		// zitErinD

		start = tijd();
		System.out.print("D: " + gr.zitErinD(65000) + "     Tijd: ");
		end = tijd();
		System.out.println((end - start));
		System.out.println("end program");

	}

	// Hulpmethode voor tijdsbepaling
	private static long tijd() {
		return System.nanoTime();
	}
}
