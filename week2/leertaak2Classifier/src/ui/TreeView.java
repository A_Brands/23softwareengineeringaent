package ui;

import classifier.DecisionTree;
import classifier.Node;

import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import java.util.LinkedList;

public class TreeView extends JPanel{
    private LinkedList<JLabel> nodes = new LinkedList<>();
    private DecisionTree tree;
    public TreeView(DecisionTree tree){
       this.tree = tree;
    	Node rootNode = tree.getRoot();
        this.loadNodes(rootNode);
        this.drawTree();
        this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
    }

    private void drawTree() {
        JTextArea label = new JTextArea(tree.toString(), 100,100);
        JScrollPane scroll = new JScrollPane (label, 
        		   JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
        this.add(scroll);
    }

    private void loadNodes(Node node){
        try {
            for (Object answer : node.getAnswers()) {
                JLabel nodeLabel = new JLabel(node.getLabel());
                Node newNode = node.follow((String)answer);
                nodes.push(nodeLabel);
                this.loadNodes(newNode);
            }
        }catch(Exception e){
            return;
        }
    }
}
