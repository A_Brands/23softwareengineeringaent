package ui;

import classifier.DecisionTree;

import javax.swing.JApplet;
import java.awt.GridLayout;
import java.io.File;
import java.net.URL;

public class ClassifierMVC extends JApplet {
    public ClassifierMVC() throws Exception {
        File jsonFile = new File(getProjectFolder() + "/data/trainingsets.json");

        // Create a new tree from a JSON file.
        DecisionTree tree = new DecisionTree(jsonFile);

        // Create the layout
        this.setLayout(new GridLayout(2,1));

        // Create the controllers
        ClassifierController controller = new ClassifierController(tree);

        // Create the views
        QuestionView questionView = new QuestionView(tree);
        TreeView  treeView = new TreeView(tree);

        // Register the views
        controller.addView(questionView);

        // Add the views
        this.add(questionView);
        this.add(treeView);
        this.add(controller);
    }

    private File getProjectFolder() {
        URL resource = getClass().getClassLoader().getResource(".");
        if (resource == null) {
            return null;
        }
        String path = resource.getPath();
        return new File(path);
    }
}
