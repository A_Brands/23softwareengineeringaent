package ui;

import classifier.DecisionTree;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;

public class QuestionView extends JPanel implements IClassifierView {
    private JLabel questionLabel;
    private DecisionTree tree;

    public QuestionView(DecisionTree tree){
        // Create the layout.
        this.setLayout(new GridLayout(1,1));

        // Store the model.
        this.tree = tree;

        // Create the question label.
        this.questionLabel = new JLabel(tree.getQuestion() + "?", SwingConstants.CENTER);
        this.add(this.questionLabel);
    }

    public void actionPerformed(ActionEvent e){
        // Handle next question events
        if (e.getActionCommand().equals("nextQuestion")) {
            this.questionLabel.setText(tree.getQuestion() + "?");
        } else if (e.getActionCommand().equals("showCategory")) {
            this.questionLabel.setText(tree.getQuestion());
        }
    }
}
