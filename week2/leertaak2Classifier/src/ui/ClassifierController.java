package ui;

import classifier.DecisionTree;

import javax.swing.JButton;
import javax.swing.JPanel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class ClassifierController extends JPanel {
    private ArrayList<IClassifierView> views = new ArrayList<>();
    private ArrayList<JButton> buttons = new ArrayList<>();
    private DecisionTree tree;

    /**
     * Creates a new classifier controller.
     *
     * @param tree
     *          The decision tree model to use.
     */
    public ClassifierController(DecisionTree tree){
        this.tree = tree;
        this.updateButtons();
    }

    /**
     * Removes the current buttons and generates the new buttons.
     */
    public void updateButtons(){
        // Remove all the current buttons.
        this.buttons.forEach(this::remove);

        // If the current node is a leaf, skip adding new buttons.
        if (!this.tree.isLeaf()) {
            // Create buttons for each possible answer
            for (String name : this.tree.getAnswers()) {
                JButton button = new JButton(name);
                this.buttons.add(button);
                button.addActionListener(e -> {
                    // Try to traverse the tree.
                    if (ClassifierController.this.tree.follow(e.getActionCommand())) {
                        if (ClassifierController.this.tree.isLeaf()) {
                            ClassifierController.this.showCategory();
                        } else {
                            // Show the next question and update the buttons
                            ClassifierController.this.nextQuestion();
                        }
                    }
                });
                this.add(button);
            }
        }

        // Validate and repaint the button view.
        this.validate();
        this.repaint();
    }

    /**
     * Sends the next question event to the views and update the buttons.
     */
    private void nextQuestion() {
        for (ActionListener view : views) {
            view.actionPerformed(new ActionEvent(ClassifierController.this,
                                                 ActionEvent.ACTION_PERFORMED,
                                                 "nextQuestion"));
        }
        ClassifierController.this.updateButtons();
    }

    /**
     * Sends the show category event to the views and update the buttons.
     */
    private void showCategory() {
        for (ActionListener view : views) {
            view.actionPerformed(new ActionEvent(ClassifierController.this,
                                                 ActionEvent.ACTION_PERFORMED,
                                                 "showCategory"));
        }
        ClassifierController.this.updateButtons();
    }

    /**
     * Adds a view to the controller.
     * The view must implement {@code ActionListener} to respond to events.
     *
     * @param view
     *          The view to add.
     */
    public void addView(IClassifierView view) {
        this.views.add(view);
    }
}
