package classifier;

import java.io.File;
import java.net.URL;

public class Run {
    public static void main(String args[]) {
        try {
            new Run();
        }catch (Exception e){
            e.printStackTrace();
            return;
        }
    }

    public Run() throws Exception {
        File jsonFile = new File(getProjectFolder() + "/data/trainingsets.json");

        DecisionTree tree = new DecisionTree(jsonFile);
        System.out.println(tree.toString());
    }

    private File getProjectFolder() {
        URL resource = getClass().getClassLoader().getResource(".");
        if (resource == null) {
            return null;
        }
        String path = resource.getPath();
        return new File(path);
    }


}
