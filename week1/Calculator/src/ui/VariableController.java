package ui;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import multiformat.Calculator;

/**
 * Controller for adding operands to or deleting operands from the Calculator
 * 
 * @author Ton Broekhuizen
 * @author Arnoud Brands
 */
public class VariableController extends JPanel implements ActionListener {

	// Used to tell the Calculator what to do
	private Calculator model;
	// Text field to input variable names
	private JTextField variable = new JTextField();
	// Button to save a variable using the first value on the stack
	private JButton save = new JButton("Save");
	// Button to load a variable onto the stack
	private JButton load = new JButton("Load");
	// Button to delete a variable
	private JButton delete = new JButton("Delete");

	public VariableController(Calculator model) {
		this.model = model;

		this.setLayout(new GridLayout(1, 4));

		// Add elements
		this.add(this.variable);
		this.add(this.save);
		this.save.addActionListener(this);
		this.add(this.load);
		this.load.addActionListener(this);
		this.add(this.delete);
		this.delete.addActionListener(this);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// See which button was clicked
		// If an Exception gets thrown, catch it and show its message in a
		// dialog box
		try {
			if (e.getSource() == this.save) {
				// Save variable in the Calculator
				if (this.model.peekOperand() == null)
					return;

				this.model.setVariable(this.variable.getText(),
						this.model.peekOperand());
				this.variable.setText("");

			} else if (e.getSource() == this.load) {
				// Load variable onto the stack
				String value = this.model.getVariable(this.variable.getText());
				if (value == null)
					return;
				this.model.addOperand(value);
			} else if (e.getSource() == this.delete) {
				this.model.setVariable(this.variable.getText(), null);
			}
		} catch (Exception ex) {
			JOptionPane.showMessageDialog(null, ex.getMessage(), ex.getClass()
					.getName(), JOptionPane.WARNING_MESSAGE);
		}
	}

}
