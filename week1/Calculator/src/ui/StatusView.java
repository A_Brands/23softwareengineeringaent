package ui;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.ScrollPaneConstants;

import multiformat.Calculator;

/**
 * View that displays the current status of the calculator (operands, variables,
 * format, base)
 * 
 * @author Ton Broekhuizen
 * @author Arnoud Brands
 */
public class StatusView extends JPanel implements ActionListener {

	// Label to display the base/format
	private JLabel settings;
	// Text area to display the operands
	private JTextArea operands;
	// Text area to display the variables
	private JTextArea variables;

	public StatusView() {
		this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));

		// Add settings label
		this.settings = new JLabel();
		this.settings.setAlignmentX(LEFT_ALIGNMENT);
		this.add(this.settings);

		// Add panel for bottom panels
		JPanel bottomPanels = new JPanel();
		bottomPanels.setLayout(new GridLayout(1, 2));
		bottomPanels.setAlignmentX(LEFT_ALIGNMENT);
		this.add(bottomPanels);

		// Add left panel for displaying operands
		JPanel leftPanel = new JPanel();
		leftPanel.setLayout(new GridLayout(1, 1));
		bottomPanels.add(leftPanel);

		// Add operands scrollable display
		this.operands = new JTextArea(10, 0);
		this.operands.setEditable(false);
		JScrollPane operandsScroll = new JScrollPane(this.operands,
				ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS,
				ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		operandsScroll.setAlignmentX(LEFT_ALIGNMENT);
		leftPanel.add(operandsScroll);

		// Add right panel for displaying variables
		JPanel rightPanel = new JPanel();
		rightPanel.setLayout(new GridLayout(1, 1));
		bottomPanels.add(rightPanel);

		// Add variables scrollable display
		this.variables = new JTextArea(10, 0);
		this.variables.setEditable(false);
		JScrollPane variablesScroll = new JScrollPane(this.variables,
				ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS,
				ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		variablesScroll.setAlignmentX(LEFT_ALIGNMENT);
		rightPanel.add(variablesScroll);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		Calculator c = (Calculator) e.getSource();
		// Update settings label
		this.settings.setText(c.getFormat().getName() + ", "
				+ c.getBase().getName());

		// Update operands display
		String operandsText = "";
		for (String s : c.getOperands()) {
			operandsText += s + "\n";
		}
		this.operands.setText(operandsText);

		// Update variables display
		String variablesText = "";
		for (String s : c.getVariables()) {
			variablesText += s + "\n";
		}
		this.variables.setText(variablesText);
	}

}
