package ui;

import java.awt.BorderLayout;
import java.awt.GridLayout;

import javax.swing.JApplet;
import javax.swing.JPanel;

import multiformat.Calculator;
import multiformat.DecimalBase;
import multiformat.FixedPointFormat;

/**
 * GUI applet for the Calculator, made in an MVC structure
 * 
 * @author Ton Broekhuizen
 * @author Arnoud Brands
 */
public class CalculatorMVC extends JApplet {

	// the model
	private Calculator model;
	// the controller for adding/deleting operands
	private OperandController operandController;
	// the controller for saving and loading variables
	private VariableController variableController;
	// the controller for doing things with operands and changing base/format
	private ButtonPanelController buttonPanelController;
	// the view that displays the current status of the calculator (operands,
	// variables, format, base)
	private StatusView statusView;
	// the view that displays the amount of calculations done
	private CounterView counterView;

	@Override
	public void init() {
		// Set window size and layout
		this.resize(420, 330);
		this.getContentPane().setLayout(new BorderLayout(10, 10));

		// Create the model
		this.model = new Calculator();

		// Add center panel for operand/variable controllers
		JPanel centerPanel = new JPanel(new GridLayout(2, 1));
		this.getContentPane().add(centerPanel, BorderLayout.CENTER);

		// Create and add operand controller
		this.operandController = new OperandController(this.model);
		centerPanel.add(this.operandController);
		this.model.addActionListener(this.operandController);

		// Create and add variable controller
		this.variableController = new VariableController(this.model);
		centerPanel.add(this.variableController);
		this.model.addActionListener(this.variableController);

		// Create and add button panel controller
		this.buttonPanelController = new ButtonPanelController(this.model);
		this.getContentPane().add(this.buttonPanelController,
				BorderLayout.SOUTH);

		// Create and add status view
		this.statusView = new StatusView();
		this.getContentPane().add(this.statusView, BorderLayout.NORTH);
		this.model.addActionListener(this.statusView);

		// Create and add counter view
		this.counterView = new CounterView();
		this.getContentPane().add(this.counterView, BorderLayout.EAST);
		this.model.addActionListener(this.counterView);

		// Call model methods so it sends events to views/controllers
		this.model.setFormat(new FixedPointFormat());
		this.model.setBase(new DecimalBase());
	}

}
