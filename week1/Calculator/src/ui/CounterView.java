package ui;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JLabel;
import javax.swing.JPanel;

import multiformat.Calculator;

/**
 * View that displays the amount of calculations done so far
 * 
 * @author Ton Broekhuizen
 * @author Arnoud Brands
 */
public class CounterView extends JPanel implements ActionListener {

	// Label that displays the amount
	private JLabel counter;

	public CounterView() {
		this.setLayout(new GridLayout(1, 1));

		// Add elements
		this.counter = new JLabel();
		this.add(this.counter);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// Read data and set the label to display it
		Calculator c = (Calculator) e.getSource();
		this.counter.setText("Calculations so far: " + c.getCalculationsDone());
	}

}
