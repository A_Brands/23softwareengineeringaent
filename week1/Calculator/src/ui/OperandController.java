package ui;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import multiformat.Calculator;

/**
 * Controller for adding operands to or deleting operands from the Calculator
 * 
 * @author Ton Broekhuizen
 * @author Arnoud Brands
 */
public class OperandController extends JPanel implements ActionListener {

	// Used to tell the Calculator what to do
	private Calculator model;
	// Text field to input operands
	private JTextField operand = new JTextField();
	// Button to add the operand to the Calculator
	private JButton add = new JButton("Add");
	// Button to delete operands from the Calculator
	private JButton delete = new JButton("Delete");

	public OperandController(Calculator model) {
		this.model = model;

		this.setLayout(new GridLayout(1, 3));

		// Add elements
		this.add(this.operand);
		this.add(this.add);
		this.add.addActionListener(this);
		this.add(this.delete);
		this.delete.addActionListener(this);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// See which button was clicked
		if (e.getSource() == this.add) {
			// Add operand to the Calculator
			// If an Exception gets thrown, catch it and show its message in a
			// dialog box
			try {
				this.model.addOperand(this.operand.getText());
				this.operand.setText("");
			} catch (Exception ex) {
				JOptionPane.showMessageDialog(null, ex.getMessage(), ex
						.getClass().getName(), JOptionPane.WARNING_MESSAGE);
			}
		} else if (e.getSource() == this.delete) {
			// Delete operand from the Calculator
			this.model.delete();
		}
	}

}
