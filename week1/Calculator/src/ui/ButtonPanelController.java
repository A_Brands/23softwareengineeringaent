package ui;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JPanel;

import multiformat.BinaryBase;
import multiformat.Calculator;
import multiformat.DecimalBase;
import multiformat.FixedPointFormat;
import multiformat.FloatingPointFormat;
import multiformat.HexBase;
import multiformat.OctalBase;
import multiformat.RationalFormat;

/**
 * Controller that tells the Calculator to do things with the operands or change
 * the base/format
 * 
 * @author Ton Broekhuizen
 * @author Arnoud Brands
 */
public class ButtonPanelController extends JPanel implements ActionListener {

	// Used to tell the Calculator what to do
	protected Calculator model;

	public ButtonPanelController(Calculator model) {
		this.model = model;

		this.setLayout(new GridLayout(3, 4));

		// buttons for working with operands
		this.addButton("+");
		this.addButton("-");
		this.addButton("*");
		this.addButton("/");

		// buttons for changing the base
		this.addButton("Dec");
		this.addButton("Bin");
		this.addButton("Oct");
		this.addButton("Hex");

		// buttons for changing the format
		this.addButton("Rat");
		this.addButton("Fixed");
		this.addButton("Float");
	}

	/**
	 * Adds a JButton to the panel.
	 * 
	 * @param caption
	 *            The text to show on the button
	 */
	protected void addButton(String caption) {
		JButton button = new JButton(caption);
		button.addActionListener(this);
		this.add(button);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// check for the button clicked
		if (e.getSource() instanceof JButton) {
			String t = ((JButton) e.getSource()).getText();
			if (t.equals("+")) {
				// add operands
				this.model.add();
			} else if (t.equals("-")) {
				// subtract operands
				this.model.subtract();
			} else if (t.equals("*")) {
				// multiply operands
				this.model.multiply();
			} else if (t.equals("/")) {
				// divide operands
				this.model.divide();
			} else if (t.equals("Dec")) {
				// set base to decimal
				this.model.setBase(new DecimalBase());
			} else if (t.equals("Bin")) {
				// set base to binary
				this.model.setBase(new BinaryBase());
			} else if (t.equals("Oct")) {
				// set base to octal
				this.model.setBase(new OctalBase());
			} else if (t.equals("Hex")) {
				// set base to hexadecimal
				this.model.setBase(new HexBase());
			} else if (t.equals("Rat")) {
				// set format to rational
				this.model.setFormat(new RationalFormat());
			} else if (t.equals("Fixed")) {
				// set format to fixed point
				this.model.setFormat(new FixedPointFormat());
			} else if (t.equals("Float")) {
				// set format to floating point
				this.model.setFormat(new FloatingPointFormat());
			}
		}
	}

}
