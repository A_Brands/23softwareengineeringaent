/*
 * (C) Copyright 2005 Davide Brugali, Marco Torchiano
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
 * 02111-1307  USA
 */
package multiformat;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Stack;

/**
 * The multiformat calculator
 */
public class Calculator {
	private Stack<Rational> operands = new Stack<Rational>();
	private Map<String, Rational> variables = new HashMap<String, Rational>();

	// The current format of the calculator
	private Format format = new FixedPointFormat();
	// The current numberbase of the calculator
	private Base base = new DecimalBase();
	// The amount of calculations done so far
	private int calculationsDone = 0;

	// listeners to send events to
	private List<ActionListener> actionListenerList = new ArrayList<ActionListener>();

	public void addOperand(String newOperand) throws FormatException {
		this.operands.push(this.format.parse(newOperand, this.base));
		this.processEvent(new ActionEvent(this, ActionEvent.ACTION_PERFORMED,
				null));
	}

	public void add() {
		if (this.operands.size() < 2)
			return;
		Rational operand1 = this.operands.pop();
		Rational operand0 = this.operands.pop();
		this.operands.push(operand0.plus(operand1));
		this.calculationsDone++;
		this.processEvent(new ActionEvent(this, ActionEvent.ACTION_PERFORMED,
				null));
	}

	public void subtract() {
		if (this.operands.size() < 2)
			return;
		Rational operand1 = this.operands.pop();
		Rational operand0 = this.operands.pop();
		this.operands.push(operand0.minus(operand1));
		this.calculationsDone++;
		this.processEvent(new ActionEvent(this, ActionEvent.ACTION_PERFORMED,
				null));
	}

	public void multiply() {
		if (this.operands.size() < 2)
			return;
		Rational operand1 = this.operands.pop();
		Rational operand0 = this.operands.pop();
		this.operands.push(operand0.mul(operand1));
		this.calculationsDone++;
		this.processEvent(new ActionEvent(this, ActionEvent.ACTION_PERFORMED,
				null));
	}

	public void divide() {
		if (this.operands.size() < 2)
			return;
		Rational operand1 = this.operands.pop();
		Rational operand0 = this.operands.pop();
		this.operands.push(operand0.div(operand1));
		this.calculationsDone++;
		this.processEvent(new ActionEvent(this, ActionEvent.ACTION_PERFORMED,
				null));
	}

	public void delete() {
		if (this.operands.isEmpty())
			return;
		this.operands.pop();
		this.processEvent(new ActionEvent(this, ActionEvent.ACTION_PERFORMED,
				null));
	}

	/**
	 * @return A list of all operands currently on the stack, formatted by
	 *         format and base
	 */
	public List<String> getOperands() {
		List<String> operands = new ArrayList<String>();
		for (Rational r : this.operands) {
			operands.add(this.format.toString(r, this.base));
		}
		return operands;
	}

	/**
	 * Gets a specific operand on the stack.
	 * 
	 * @param index
	 *            The index of the operand to get
	 * @return The operand on that position of the stack, formatted by format
	 *         and base
	 */
	public String getOperand(int index) {
		return this.format.toString(this.operands.get(index), this.base);
	}

	/**
	 * @return The value of the first operand on the stack
	 */
	public String peekOperand() {
		if (this.operands.isEmpty())
			return null;
		return this.format.toString(this.operands.peek(), this.base);
	}

	/**
	 * @return A list of all variables currently stored, formatted by format and
	 *         base
	 */
	public List<String> getVariables() {
		List<String> variables = new ArrayList<String>();
		for (Entry<String, Rational> e : this.variables.entrySet()) {
			variables.add(e.getKey() + " = "
					+ this.format.toString(e.getValue(), this.base));
		}
		return variables;
	}

	/**
	 * Gets the value of a specific variable by name.
	 * 
	 * @param var
	 *            The name of the variable whose value to get
	 * @return The value of the variable, formatted by format and base, or null
	 *         if no variable was found
	 */
	public String getVariable(String var) {
		Rational value = this.variables.get(var);
		if (value == null)
			return null;
		return this.format.toString(value, this.base);
	}

	/**
	 * Sets the value of a specific variable.
	 * 
	 * @param var
	 *            The name of the variable whose value to set
	 * @param value
	 *            The value to set the variable to
	 */
	public void setVariable(String var, String value) throws FormatException {
		if (value == null || value.equals(""))
			this.variables.remove(var);
		else
			this.variables.put(var, this.format.parse(value, this.getBase()));

		this.processEvent(new ActionEvent(this, ActionEvent.ACTION_PERFORMED,
				null));
	}

	public void setBase(Base newBase) {
		this.base = newBase;
		this.processEvent(new ActionEvent(this, ActionEvent.ACTION_PERFORMED,
				null));
	}

	public Base getBase() {
		return this.base;
	}

	public void setFormat(Format newFormat) {
		this.format = newFormat;
		this.processEvent(new ActionEvent(this, ActionEvent.ACTION_PERFORMED,
				null));
	}

	public Format getFormat() {
		return this.format;
	}

	/**
	 * @return The amount of calculations done so far
	 */
	public int getCalculationsDone() {
		return this.calculationsDone;
	}

	/**
	 * Adds a listener to fire events at.
	 * 
	 * @param l
	 *            The listener to add
	 */
	public void addActionListener(ActionListener l) {
		this.actionListenerList.add(l);
	}

	/**
	 * Fires an event at all registered ActionListeners.
	 * 
	 * @param e
	 *            The event to fire
	 */
	private void processEvent(ActionEvent e) {
		for (ActionListener l : this.actionListenerList)
			l.actionPerformed(e);
	}

}