package multiformat;

/**
 * Used for representing numbers in the octal numeral system.
 * 
 * @author Ton Broekhuizen
 * @author Arnoud Brands
 * @version 1.0
 */
public class OctalBase extends Base {

	public OctalBase() {
		super("oct", 8, "01234567");
	}

}
