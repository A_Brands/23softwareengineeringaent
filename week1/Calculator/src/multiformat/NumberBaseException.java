package multiformat;

/**
 * To be thrown when a digit is entered that does not belong to the current
 * numeral system.
 * 
 * @author Ton Broekhuizen
 * @author Arnoud Brands
 * @version 1.0
 * 
 */
public class NumberBaseException extends RuntimeException {

	public NumberBaseException(String digit) {
		super("Digit " + digit + " is of incorrect number base");
	}

}
