/*
 * (C) Copyright 2005 Davide Brugali, Marco Torchiano
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
 * 02111-1307  USA
 */
package multiformat;

/**
 * Class representing a rational ('breuk').
 * 
 * @author J.Balj�
 * @author Ton Broekhuizen
 * @author Arnoud Brands
 * @version 1.0
 */
public class Rational {
	static final double PRECISION = 10;
	static final double EPSILON = Math.pow(10, -PRECISION);

	double numerator = 0.0; // teller
	double denominator = 1.0; // noemer

	/**
	 * Creates a new Rational.
	 * 
	 * @param num
	 *            Numerator of the fraction
	 * @param den
	 *            Denominator of the fraction
	 */
	public Rational(double num, double den) {
		this.numerator = num;
		this.denominator = den;
		this.simplify();
	}

	/**
	 * Creates a new Rational with the default value (0/1).
	 */
	public Rational() {
	}

	/**
	 * Creates a new Rational from a double.
	 * 
	 * @param number
	 *            The number to convert into a Rational
	 */
	public Rational(double number) {
		this.numerator = number;
		this.denominator = 1.0;
		this.canonical();
		this.simplify();
	}

	/**
	 * Gets rid of any decimals in the numerator. For example, 12.5/1.0 becomes
	 * 125.0/10.0 (Note that any decimals in the denominator aren't handled,
	 * like 10/0.5. This seems to be an omission.)
	 * 
	 * @see test.TestRational
	 */
	public void canonical() {
		double num = Math.abs(this.numerator);
		double decimal = num - Math.floor(num);
		int num_digits = 0;

		while (decimal > EPSILON && num_digits < PRECISION) {
			num = num * 10;
			decimal = num - Math.floor(num);
			num_digits++;
		}

		this.numerator = this.numerator * Math.pow(10.0, num_digits);
		this.denominator = this.denominator * Math.pow(10.0, num_digits);
	}

	/**
	 * Simplifies the Rational. For example, 125/10 becomes 25/2.
	 * 
	 * @see test.TestRational
	 */
	public void simplify() {
		// Take the smallest from the two (10.0)
		double divisor = Math.min(Math.abs(this.numerator), this.denominator);
		// Step from 10.0 to 9.0 to ... 1.0
		for (; divisor > 1.0; divisor -= 1.0) {
			double rn = Math.abs(Math.IEEEremainder(Math.abs(this.numerator),
					divisor));
			double rd = Math.abs(Math.IEEEremainder(this.denominator, divisor));
			// If both the numerator and denominator have a very small remainder
			// then they can both be divided by devisor (in our example 5).
			if (rn < EPSILON && rd < EPSILON) {
				this.numerator /= divisor;
				this.denominator /= divisor;
				divisor = Math.min(Math.abs(this.numerator), this.denominator);
			}
		}
	}

	/**
	 * Calculates the sum of this Rational and another Rational.
	 * 
	 * @param other
	 *            The Rational to calculate the sum with
	 * @return A new Rational representing the sum
	 */
	public Rational plus(Rational other) {
		if (this.denominator == other.denominator)
			return new Rational(this.numerator + other.numerator,
					other.denominator);
		else
			// a/x + b/y =
			// (breuken gelijknamig maken)
			// a*y/x*y + b*x/x*y = (a*y + b*x)/x*y
			return new Rational(this.numerator * other.denominator
					+ this.denominator * other.numerator, this.denominator
					* other.denominator);
	}

	/**
	 * Subtracts the value of this Rational with the value of another Rational.
	 * 
	 * @param other
	 *            The Rational whose value is subtracted from this Rational's
	 *            value
	 * @return A new Rational representing the result of the subtraction
	 */
	public Rational minus(Rational other) {
		if (this.denominator == other.denominator)
			return new Rational(this.numerator - other.numerator,
					this.denominator);
		else
			return new Rational(this.numerator * other.denominator
					- this.denominator * other.numerator, this.denominator
					* other.denominator);
	}

	/**
	 * Multiplies the value of this Rational with the value of another Rational.
	 * 
	 * @param other
	 *            The Rational whose value is multiplied with this Rational's
	 *            value
	 * @return A new Rational representing the result of the multiplication
	 */
	public Rational mul(Rational other) {
		return new Rational(this.numerator * other.numerator, this.denominator
				* other.denominator);
	}

	/**
	 * Divides the value of this Rational by the value of another Rational.
	 * 
	 * @param other
	 *            The Rational whose value is used to divide this Rational's
	 *            value
	 * @return A new Rational representing the result of the division
	 */
	public Rational div(Rational other) {
		if (other.numerator <= 0 || other.denominator <= 0) {
			Rational rat = new Rational();
			rat.copyOf(this);
			return rat;
		}
		return new Rational(this.numerator * other.denominator,
				this.denominator * other.numerator);
	}

	/**
	 * Copies the value of another Rational to this Rational.
	 * 
	 * @param other
	 *            The Rational whose value is to be copied
	 */
	public void copyOf(Rational other) {
		this.numerator = other.numerator;
		this.denominator = other.denominator;
	}

	// Added getters and setters for unittesting purposes.
	/**
	 * Gets the numerator of this Rational.
	 * 
	 * @return The numerator of this Rational
	 */
	public double getNumerator() {
		return this.numerator;
	}

	/**
	 * Gets the denominator of this Rational.
	 * 
	 * @return The denominator of this Rational
	 */
	public double getDenominator() {
		return this.denominator;
	}

	/**
	 * Sets the numerator of this Rational.
	 * 
	 * @param num
	 *            The numerator to set
	 */
	public void setNumerator(double num) {
		this.numerator = num;
	}

	/**
	 * Sets the denominator of this Rational.
	 * 
	 * @param num
	 *            The denominator to set
	 */
	public void setDenominator(double den) {
		this.denominator = den;
	}
}
