package test;

import static org.junit.Assert.assertEquals;
import multiformat.Rational;

import org.junit.Before;
import org.junit.Test;

/**
 * JUnit Testcase to test Rational. Note that this class uses 'annotations' (the
 * 
 * @...). This is a Java 1.5 feature.
 * 
 * @author gebruiker
 * 
 */
public class TestRational {
	Rational r;

	@Before
	public void setUp() {
		this.r = new Rational();
	}

	@Test
	public void testSimplify() {
		this.r.setNumerator(25);
		this.r.setDenominator(5);
		this.r.simplify();

		assertEquals(5.0, this.r.getNumerator(), 0.0);
		assertEquals(1.0, this.r.getDenominator(), 0.0);

		this.r.setNumerator(10);
		this.r.setDenominator(0.5);
		this.r.simplify();

		assertEquals(10.0, this.r.getNumerator(), 0.0);
		assertEquals(0.5, this.r.getDenominator(), 0.0);
	}

	@Test
	public void testCanonical() {
		this.r.setNumerator(12.5);
		this.r.setDenominator(1.0);
		this.r.canonical();

		assertEquals(125.0, this.r.getNumerator(), 0.0);
		assertEquals(10.0, this.r.getDenominator(), 0.0);

		this.r.setNumerator(12.5);
		this.r.setDenominator(0.01);
		this.r.canonical();

		assertEquals(125.0, this.r.getNumerator(), 0.0);
		assertEquals(0.1, this.r.getDenominator(), 0.0);
	}

	@Test
	public void testCanonicalAndSimplify() {
		this.r.setNumerator(12.5);
		this.r.setDenominator(1.0);
		this.r.canonical();
		this.r.simplify();

		assertEquals(25.0, this.r.getNumerator(), 0.0);
		assertEquals(2.0, this.r.getDenominator(), 0.0);
	}

	@Test
	public void testDivisionByZero() {
		this.r.setNumerator(5.0);
		this.r.setDenominator(1.0);

		Rational r2 = new Rational(0.0, 0.0);
		this.r = this.r.div(r2);

		assertEquals(5.0, this.r.getNumerator(), 0.0);
		assertEquals(1.0, this.r.getDenominator(), 0.0);
	}

}
