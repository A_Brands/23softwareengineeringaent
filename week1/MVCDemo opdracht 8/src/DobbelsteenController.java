import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JPanel;

@SuppressWarnings("serial")
public class DobbelsteenController extends JPanel implements ActionListener {

	DobbelsteenModel model;
	private JButton hoger = new JButton("hoger");
	private JButton lager = new JButton("lager");
	private JButton gooi = new JButton("gooi");

	public DobbelsteenController(DobbelsteenModel model) {
		this.model = model;
		this.add(this.hoger);
		this.hoger.addActionListener(this);
		this.add(this.lager);
		this.lager.addActionListener(this);
		this.add(this.gooi);
		this.gooi.addActionListener(this);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == this.hoger) {
			this.model.verhoog();
		}
		if (e.getSource() == this.lager) {
			this.model.verlaag();
		}
		if (e.getSource() == this.gooi) {
			this.model.gooi();
		}
	}

	@Override
	public Dimension getPreferredSize() {
		return new Dimension(50, 50);
	}

}
