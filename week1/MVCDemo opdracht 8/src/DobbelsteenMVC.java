import java.awt.BorderLayout;
import java.awt.Color;

import javax.swing.JApplet;

@SuppressWarnings("serial")
public class DobbelsteenMVC extends JApplet {
	private DobbelsteenModel model;
	private TextView textView;
	private DobbelsteenView dobbelsteenView;
	private StatsView statsView;
	private DobbelsteenController controller;

	@Override
	public void init() {
		this.resize(250, 350);

		// Maak het model
		this.model = new DobbelsteenModel();

		// Maak de controller en geef hem het model
		this.controller = new DobbelsteenController(this.model);
		this.controller.setBackground(Color.cyan);
		this.getContentPane().add(this.controller, BorderLayout.NORTH);

		// Maak de views
		this.dobbelsteenView = new DobbelsteenView(Color.red);
		this.dobbelsteenView.setBackground(Color.black);
		this.getContentPane().add(this.dobbelsteenView, BorderLayout.CENTER);

		this.textView = new TextView();
		this.textView.setBackground(Color.green);
		this.getContentPane().add(this.textView, BorderLayout.SOUTH);

		this.statsView = new StatsView();
		this.getContentPane().add(this.statsView, BorderLayout.EAST);

		// Registreer de views bij het model
		this.model.addActionListener(this.textView);
		this.model.addActionListener(this.dobbelsteenView);
		this.model.addActionListener(this.statsView);

		// Eerste worp
		this.model.gooi();
	}
}
