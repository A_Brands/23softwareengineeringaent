import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JPanel;
import javax.swing.JTextField;

@SuppressWarnings("serial")
public class TextView extends JPanel implements ActionListener {

	private final JTextField valueField = new JTextField();

	public TextView() {
		this.setLayout(new FlowLayout());
		this.add(this.valueField);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		DobbelsteenModel d = (DobbelsteenModel) e.getSource();
		this.valueField.setText(String.valueOf(d.getWaarde()));
	}

	@Override
	public Dimension getPreferredSize() {
		return new Dimension(50, 50);
	}

}
