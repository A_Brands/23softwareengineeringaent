import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

// NOTA BENE: Deze code is niet thread-safe omdat jullie dat in de 1e week nog niet kennen. 
// Zie paragraaf 30.2 voor de thread-safe implementatie.
public class DobbelsteenModel {

	private int waarde;
	private ArrayList<ActionListener> actionListenerList = new ArrayList<ActionListener>();

	public DobbelsteenModel() {
		this.waarde = (int) (Math.random() * 6 + 1);
	}

	public int getWaarde() {
		return this.waarde;
	}

	public void verhoog() {
		this.waarde++;
		if (this.waarde > 6)
			this.waarde = 1;

		// Merk op dat we de 3e String-parameter van de constructor van de
		// ActionEvent niet invullen.
		// In dit geval zou je die kunnen gebruiken om de nieuwe
		// dobbelsteenwaarde mee te geven
		// aan de ActionListener. Dan hoeft de ActionListener niet met
		// e.getSource() weer naar
		// het model toe te gaan.
		this.processEvent(new ActionEvent(this, ActionEvent.ACTION_PERFORMED,
				null));
	}

	public void verlaag() {
		this.waarde--;
		if (this.waarde < 1)
			this.waarde = 6;
		this.processEvent(new ActionEvent(this, ActionEvent.ACTION_PERFORMED,
				null));
	}

	public void gooi() {
		this.waarde = (int) (Math.random() * 6 + 1);
		this.processEvent(new ActionEvent(this, ActionEvent.ACTION_PERFORMED,
				null));
	}

	public void addActionListener(ActionListener l) {
		this.actionListenerList.add(l);
	}

	public void removeActionListener(ActionListener l) {
		if (this.actionListenerList.contains(l))
			this.actionListenerList.remove(l);
	}

	private void processEvent(ActionEvent e) {
		// Hieronder gebruiken we het nieuwe Java "foreach" statement.
		// Lees het als: "for each ActionListener in actionListenerList do ..."
		// Je kunt ook een for-lus of een iterator gebruiken, maar foreach is
		// het elegantste.
		for (ActionListener l : this.actionListenerList)
			l.actionPerformed(e);
	}

}
