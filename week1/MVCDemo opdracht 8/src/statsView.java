import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JLabel;
import javax.swing.JPanel;

@SuppressWarnings("serial")
public class StatsView extends JPanel implements ActionListener {

	private int mainCounter = 0;
	private JLabel mainCounterLabel = new JLabel();
	private int[] counters = new int[6];
	private JLabel[] counterLabels = new JLabel[6];

	public StatsView() {
		this.setLayout(new GridLayout(7, 2, 10, 0));

		this.add(new JLabel("Worpen:"));
		this.add(this.mainCounterLabel);
		for (int i = 0; i < this.counterLabels.length; i++) {
			this.add(new JLabel((i + 1) + ":"));
			this.add(this.counterLabels[i] = new JLabel());
		}
		this.updateCounterLabels();
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		DobbelsteenModel d = (DobbelsteenModel) e.getSource();
		this.mainCounter++;
		this.counters[d.getWaarde() - 1]++;
		this.updateCounterLabels();
	}

	private void updateCounterLabels() {
		this.mainCounterLabel.setText(String.valueOf(this.mainCounter));
		for (int i = 0; i < this.counterLabels.length; i++)
			this.counterLabels[i].setText(this.counters[i] + " keer");
	}

}
