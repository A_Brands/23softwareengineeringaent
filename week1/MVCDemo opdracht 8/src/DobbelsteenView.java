import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JPanel;

@SuppressWarnings("serial")
public class DobbelsteenView extends JPanel implements ActionListener {

	private final Color color;
	private int value;

	public DobbelsteenView(Color c) {
		this.color = c;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		this.value = ((DobbelsteenModel) e.getSource()).getWaarde();
		this.repaint();
	}

	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.setColor(this.color);
		g.fillRoundRect(5, 5, 70, 70, 10, 10);
		g.setColor(Color.black);
		switch (this.value) {
		case 1:
			g.fillOval(34, 34, 10, 10);
			break;
		case 2:
			g.fillOval(10, 10, 10, 10);
			g.fillOval(60, 60, 10, 10);
			break;
		case 3:
			g.fillOval(10, 10, 10, 10);
			g.fillOval(34, 34, 10, 10);
			g.fillOval(60, 60, 10, 10);
			break;
		case 4:
			g.fillOval(10, 10, 10, 10);
			g.fillOval(10, 60, 10, 10);
			g.fillOval(60, 10, 10, 10);
			g.fillOval(60, 60, 10, 10);
			break;
		case 5:
			g.fillOval(10, 10, 10, 10);
			g.fillOval(10, 60, 10, 10);
			g.fillOval(60, 10, 10, 10);
			g.fillOval(60, 60, 10, 10);
			g.fillOval(34, 34, 10, 10);
			break;
		case 6:
			g.fillOval(10, 10, 10, 10);
			g.fillOval(10, 60, 10, 10);
			g.fillOval(60, 10, 10, 10);
			g.fillOval(60, 60, 10, 10);
			g.fillOval(10, 34, 10, 10);
			g.fillOval(60, 34, 10, 10);
			break;
		}
	}

	@Override
	public Dimension getPreferredSize() {
		return new Dimension(80, 80);
	}

}
