import java.util.Arrays;

public class ArraySizeExceptionDemo {

	/**
	 * Calculates the sum of two arrays.
	 * 
	 * @param array1
	 *            The first array
	 * @param array2
	 *            The second array
	 * @return A new array that contains the sums of the input arrays' elements
	 * @throws ArraySizeException
	 *             when the arrays are not of equal length
	 */
	public static String sumOfArrayVars(int[] array1, int[] array2)
			throws ArraySizeException {
		if (array1.length != array2.length) {
			throw new ArraySizeException(array1, array2);
		}
		// array1 and array2 have the same size, define an array to collect
		// the sums with length of the arrays 1 and 2
		int[] sum = new int[array1.length];
		for (int i = 0; i < array1.length; i++) {
			sum[i] = array1[i] + array2[i];
		}
		return Arrays.toString(sum);
	}

	public static void main(String[] args) {
		try {
			System.out.println(sumOfArrayVars(new int[] { 1, 2, 3, 4, 5, 6, 7,
					8, 9 }, new int[] { 9, 8, 7, 6, 5, 4, 3, 2 }));
		} catch (ArraySizeException ase) {
			ase.printStackTrace();
		}
	}

	public static class ArraySizeException extends Exception {
		public ArraySizeException(int[] array1, int[] array2) {
			super("Array length is not equal: " + array1.length + " and "
					+ array2.length);
		}
	}

}
