package longToString;

public class Main {
	
	public static void main(String[] args)
	{
		//getal en radix

		int radix = 16;
		long getal = 3405691582L;
		
		String value = Long.toString(getal, radix);
		System.out.println(value);
	}
}
