package ttt;

import java.util.Scanner;

/*
 * Textual User Interface
 */
class Tui {
	private Scanner reader = new Scanner(System.in);
	private TicTacToe t;

	public Tui() {
		do {
			System.out.println("*** new Game ***\n");
			this.t = new TicTacToe();
			if (this.t.computerPlays())
				System.out.println("I start:\n");
			else
				System.out.println("You start:\n");
			while (!this.t.gameOver()) {
				this.t.playMove(this.move());
				System.out.println(this.t);
			}
			System.out.println("Game over " + this.t.winner() + " wins");
		} while (this.nextGame());
	}

	public static void main(String[] args) {
		new Tui();
	}

	private int move() {
		if (this.t.computerPlays()) {
			int compMove = this.t.chooseMove();
			System.out.println("Computer Move = " + compMove);
			return compMove;
		} else {
			int humanMove;
			do {
				System.out.print("Human move    = ");
				// enter integer for the position on the tictactoe board
				// 012
				// 345
				// 678
				humanMove = this.reader.nextInt();
			} while (!this.t.moveOk(humanMove));
			return humanMove;
		}
	}

	private boolean nextGame() {
		Character yn;
		do {
			System.out.print("next Game? enter Y/N: ");
			yn = (this.reader.next()).charAt(0);
			System.out.println("" + yn);
		} while (!(yn == 'Y' || yn == 'y' || yn == 'N' || yn == 'n'));
		return yn == 'Y' || yn == 'y';
	}
}
