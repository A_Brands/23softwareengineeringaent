package ttt;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

class TicTacToe {
	public static final int EMPTY = 0;
	public static final int HUMAN = 1;
	public static final int COMPUTER = 2;

	public static final int HUMAN_WIN = 0;
	public static final int DRAW = 1;
	public static final int UNCLEAR = 2;
	public static final int COMPUTER_WIN = 3;

	private int[][] board = new int[3][3];
	private Random random = new Random();
	private int side = HUMAN + this.random.nextInt(2);
	private int position = UNCLEAR;
	private char computerChar, humanChar;

	// Constructor
	public TicTacToe() {
		this.clearBoard();
		this.initSide();
	}

	private void initSide() {
		if (this.side == COMPUTER) {
			this.computerChar = 'X';
			this.humanChar = 'O';
		} else {
			this.computerChar = 'O';
			this.humanChar = 'X';
		}
	}

	public void setComputerPlays() {
		this.side = COMPUTER;
		this.initSide();
	}

	public void setHumanPlays() {
		this.side = HUMAN;
		this.initSide();
	}

	public boolean computerPlays() {
		return this.side == COMPUTER;
	}

	public int chooseMove() {
		Pos best = this.chooseMove(COMPUTER, 3);
		return best.row * 3 + best.column;
	}

	// Find optimal move
	private Pos chooseMove(int side, int depth) {
		// Get all empty positions on the board
		List<Pos> positions = this.getEmptyPositions();

		// We want to maximize our score while minimizing the opponent's
		int bestScore = (side == COMPUTER) ? Integer.MIN_VALUE
				: Integer.MAX_VALUE;
		int currentScore;
		int bestRow = -1;
		int bestCol = -1;

		// Store who is the opponent
		int opp = (side == COMPUTER) ? HUMAN : COMPUTER;

		// Check if there are any positions left
		if (positions.isEmpty() || depth == 0) {
			// If not, return the score
			bestScore = this.positionValue();
		} else {
			// Iterate over the possible positions
			for (Pos pos : positions) {
				// Execute the move
				this.place(pos.row, pos.column, side);

				if (side == COMPUTER) {
					// Call the method recursively to get us the maximum score
					currentScore = this.chooseMove(opp, depth - 1).value;
					// Check if the next score is higher than the current best
					if (currentScore > bestScore) {
						bestScore = currentScore;
						bestRow = pos.row;
						bestCol = pos.column;
					}
				} else {
					// Call the method recursively to get the opponent the
					// minimum score
					currentScore = this.chooseMove(this.side, depth - 1).value;
					// Check if the next score is lower than the current best
					if (currentScore < bestScore) {
						bestScore = currentScore;
						bestRow = pos.row;
						bestCol = pos.column;
					}
				}
				// Undo the previous move
				this.place(pos.row, pos.column, EMPTY);
			}
		}

		return new Pos(bestRow, bestCol, bestScore);
	}

	private List<Pos> getEmptyPositions() {
		List<Pos> positions = new ArrayList<Pos>();

		// Look for empty positions on the board
		for (int row = 0; row < this.board.length; row++) {
			for (int column = 0; column < this.board[row].length; column++) {
				if (this.squareIsEmpty(row, column)) {
					positions.add(new Pos(row, column));
				}
			}
		}

		return positions;
	}

	// check if move ok
	public boolean moveOk(int move) {
		return (move >= 0 && move <= 8 && this
				.squareIsEmpty(move / 3, move % 3));
	}

	// play move
	public void playMove(int move) {
		this.place(move / 3, move % 3, this.side);
		if (this.side == COMPUTER)
			this.side = HUMAN;
		else
			this.side = COMPUTER;
	}

	// Simple supporting routines
	private void clearBoard() {
		this.board = new int[3][3];
	}

	private boolean boardIsFull() {
		for (int[] row : this.board) {
			for (int i : row) {
				if (i == EMPTY) {
					return false;
				}
			}
		}
		return true;
	}

	// Returns whether 'side' has won in this position
	public boolean isAWin(int side) {

		// Left column
		if (this.board[0][0] == side && this.board[0][1] == side
				&& this.board[0][2] == side)
			return true;

		// Middle column
		if (this.board[1][0] == side && this.board[1][1] == side
				&& this.board[1][2] == side)
			return true;

		// Right column
		if (this.board[2][0] == side && this.board[2][1] == side
				&& this.board[2][2] == side)
			return true;

		// Top row
		if (this.board[0][0] == side && this.board[1][0] == side
				&& this.board[2][0] == side)
			return true;

		// Middle row
		if (this.board[0][1] == side && this.board[1][1] == side
				&& this.board[2][1] == side)
			return true;

		// Bottom row
		if (this.board[0][2] == side && this.board[1][2] == side
				&& this.board[2][2] == side)
			return true;

		// Diagonal top left to bottom right
		if (this.board[0][0] == side && this.board[1][1] == side
				&& this.board[2][2] == side)
			return true;

		// Diagonal top right to bottom left
		if (this.board[2][0] == side && this.board[1][1] == side
				&& this.board[0][2] == side)
			return true;

		return false;
	}

	// Play a move, possibly clearing a square
	public void place(int row, int column, int piece) {
		this.board[row][column] = piece;
	}

	private boolean squareIsEmpty(int row, int column) {
		return this.board[row][column] == EMPTY;
	}

	// Compute static value of current position (win, draw, etc.)
	public int positionValue() {
		if (this.isAWin(HUMAN)) {
			return HUMAN_WIN;
		} else if (this.isAWin(COMPUTER)) {
			return COMPUTER_WIN;
		} else if (this.boardIsFull()) {
			return DRAW;
		}
		return UNCLEAR;
	}

	@Override
	public String toString() {
		String display = "";

		for (int[] row : this.board) {
			for (int i : row) {
				if (i == COMPUTER)
					display += this.computerChar;
				else if (i == HUMAN)
					display += this.humanChar;
				else
					display += ".";
			}
			display += '\n';
		}

		return display;
	}

	public boolean gameOver() {
		this.position = this.positionValue();
		return this.position != UNCLEAR;
	}

	public String winner() {
		if (this.position == COMPUTER_WIN)
			return "computer";
		else if (this.position == HUMAN_WIN)
			return "human";
		else
			return "nobody";
	}

	private class Pos {
		public final int row;
		public final int column;
		public final int value;

		public Pos(int row, int column, int value) {
			this.row = row;
			this.column = column;
			this.value = value;
		}

		public Pos(int row, int column) {
			this(row, column, UNCLEAR);
		}
	}

}
