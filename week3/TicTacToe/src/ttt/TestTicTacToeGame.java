package ttt;

import junit.framework.TestCase;

public class TestTicTacToeGame extends TestCase {

	public void testPositionValueIsAWin() {
		// UNCLEAR
		// neither won (yet)
		// [ X, -, O ]
		// [ -, -, - ]
		// [ -, -, - ]
		TicTacToe ttt = new TicTacToe();
		ttt.setHumanPlays();
		ttt.playMove(0);
		ttt.playMove(2);
		assertEquals(TicTacToe.UNCLEAR, ttt.positionValue());
		assertEquals(false, ttt.isAWin(TicTacToe.HUMAN));
		assertEquals(false, ttt.isAWin(TicTacToe.COMPUTER));

		// HUMAN_WIN
		// state is a win for human
		// [ X, -, O ]
		// [ -, X, O ]
		// [ -, -, X ]
		ttt = new TicTacToe();
		ttt.setHumanPlays();
		ttt.playMove(0);
		ttt.playMove(2);
		ttt.playMove(4);
		ttt.playMove(5);
		ttt.playMove(8);
		assertEquals(TicTacToe.HUMAN_WIN, ttt.positionValue());
		assertEquals(true, ttt.isAWin(TicTacToe.HUMAN));
		assertEquals(false, ttt.isAWin(TicTacToe.COMPUTER));

		// COMPUTER_WIN
		// state is a win for computer
		// [ X, -, O ]
		// [ -, X, O ]
		// [ -, X, O ]
		ttt = new TicTacToe();
		ttt.setHumanPlays();
		ttt.playMove(0);
		ttt.playMove(2);
		ttt.playMove(4);
		ttt.playMove(5);
		ttt.playMove(7);
		ttt.playMove(8);
		assertEquals(TicTacToe.COMPUTER_WIN, ttt.positionValue());
		assertEquals(false, ttt.isAWin(TicTacToe.HUMAN));
		assertEquals(true, ttt.isAWin(TicTacToe.COMPUTER));

		// DRAW
		// state is a draw, neither win
		// [ X, X, O ]
		// [ O, O, X ]
		// [ X, O, X ]
		ttt = new TicTacToe();
		ttt.setHumanPlays();
		ttt.playMove(0);
		ttt.playMove(4);
		ttt.playMove(1);
		ttt.playMove(2);
		ttt.playMove(6);
		ttt.playMove(3);
		ttt.playMove(5);
		ttt.playMove(7);
		ttt.playMove(8);
		assertEquals(TicTacToe.DRAW, ttt.positionValue());
		assertEquals(false, ttt.isAWin(TicTacToe.HUMAN));
		assertEquals(false, ttt.isAWin(TicTacToe.COMPUTER));
	}

	public void testChooseMove() {

		// INSTANT WIN
		// [ O, X, X ]
		// [ X, O, X ]
		// [ X, X,(O)]
		TicTacToe ttt = new TicTacToe();
		ttt.place(0, 0, TicTacToe.COMPUTER);
		ttt.place(1, 1, TicTacToe.COMPUTER);
		ttt.place(1, 0, TicTacToe.HUMAN);
		ttt.place(2, 0, TicTacToe.HUMAN);
		ttt.place(0, 1, TicTacToe.HUMAN);
		ttt.place(2, 1, TicTacToe.HUMAN);
		ttt.place(0, 2, TicTacToe.HUMAN);
		ttt.place(1, 2, TicTacToe.HUMAN);
		assertEquals(8, ttt.chooseMove());

	}

}
