package dynProg.solvers;

import java.util.Random;

import dynProg.Solver;

public class RecursiveSolver implements Solver {

	private static Random rand = new Random();

	@Override
	public boolean solve(int[] numbers, int sum) {
		// Base case: Empty set can only have 0 as sum
		if (numbers.length == 0) {
			return sum == 0;
		}

		// Remove a random value from the set of numbers
		int randomKey = rand.nextInt(numbers.length);
		int[] subset = remove(numbers, randomKey);
		// See if the removal of the value matters or not
		return this.solve(subset, sum - numbers[randomKey])
				|| this.solve(subset, sum);
	}

	// Removes a value from an array
	public static int[] remove(int[] input, int toRemove) {
		int[] result = new int[input.length - 1];

		int j = 0;
		for (int i = 0; i < input.length; i++) {
			if (i == toRemove) {
				continue;
			}
			result[j] = input[i];
			j++;
		}

		return result;
	}

}
