package dynProg.solvers;

import dynProg.Solver;

public class BottomUpSolver implements Solver {

	@Override
	public boolean solve(int[] numbers, int sum) {
		// Initialize the matrix
		boolean[][] matrix = new boolean[numbers.length + 1][sum + 1];

		// Iterate through all the rows in the matrix
		for (int i = 1; i < matrix.length; i++) {
			// Get the current row
			boolean[] row = matrix[i];

			// Get the set of numbers for this row
			int[] rowNumbers = new int[i + 1];
			for (int j = 1; j < rowNumbers.length; j++) {
				rowNumbers[j] = numbers[j - 1];
			}

			// Iterate through all the cells in this row
			for (int j = 1; j < row.length; j++) {
				// Set cell to true if the set of numbers for this row contains
				// the column value
				for (int k : rowNumbers) {
					row[j] = j == k;
				}
				if (row[j]) {
					continue;
				}

				if (i > 1) {
					// Set cell to true if the above cell is also true
					row[j] |= matrix[i - 1][j];
					if (row[j]) {
						continue;
					}

					// Set to true if the column value is a sum of the other
					// numbers in the row
					for (int k = 1; k < row.length; k++) {
						if (matrix[i - 1][k]) {
							row[j] |= rowNumbers[i] + k == j;
						}
					}
				}
			}
		}

		// Look up the bottom-rightmost value, this is the result
		return matrix[numbers.length][sum];
	}

}
