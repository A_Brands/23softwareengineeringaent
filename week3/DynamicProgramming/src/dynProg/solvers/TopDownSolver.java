package dynProg.solvers;

import dynProg.Solver;

public class TopDownSolver implements Solver {

	private boolean noReset = false;
	private int[][] matrix;
	private static final int UNDEFINED = 0;
	private static final int FALSE = 1;
	private static final int TRUE = 2;

	@Override
	public boolean solve(int[] numbers, int sum) {
		// Initialize or reset the matrix
		// Only reset the matrix when not told not to
		if (!this.noReset) {
			this.matrix = new int[numbers.length + 1][sum + 1];
		}
		this.noReset = false;

		// Empty set can only have 0 as sum
		if (numbers.length == 0) {
			return sum == 0;
		}

		if (numbers.length == 1) {
			// We are at the topmost row
			// The sum can only be the value of the single number in the set
			if (numbers[numbers.length - 1] == sum) {
				return true;
			}
		} else {
			// Get the set of the row above
			int[] subset = remove(numbers, numbers.length - 1);
			// See if the cell above is already true, if so this cell is also
			// true
			boolean up = val(subset, sum);
			if (up) {
				return true;
			}

			// See if the current sum is true with the number added to the set
			// for this row
			for (int i = 1; i <= sum; i++) {
				for (int j : numbers) {
					if (val(subset, i) && j + i == sum) {
						return true;
					}
				}
			}
		}

		return false;
	}

	private boolean val(int[] row, int col) {
		// If the value being looked up is already calculated, return it
		if (this.matrix[row.length][col] == UNDEFINED) {
			// Otherwise, solve it and save it in the matrix
			this.noReset = true;
			this.matrix[row.length][col] = this.solve(row, col) ? TRUE : FALSE;
		}
		// Return the value translated to a boolean
		return this.matrix[row.length][col] == TRUE;
	}

	// Removes a value from an array
	public static int[] remove(int[] input, int toRemove) {
		int[] result = new int[input.length - 1];

		int j = 0;
		for (int i = 0; i < input.length; i++) {
			if (i == toRemove) {
				continue;
			}
			result[j] = input[i];
			j++;
		}

		return result;
	}

}
